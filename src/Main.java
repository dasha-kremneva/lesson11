package src;

public class Main {

    public static void main(String[] args) {

        //ПЕРВОЕ ЗАДАНИЕ
//        FixedThreadPool pool = new FixedThreadPool(4);
//        for (int i = 1; i <= 50; i++)
//            pool.execute(new Task(i));

        //ВТОРОЕ ЗАДАНИЕ
        ScalableThreadPool pool2 = new ScalableThreadPool(1, 8);
        for (int i = 1; i <= 50; i++) {
            pool2.execute(new Task(i));
        }

    }
}

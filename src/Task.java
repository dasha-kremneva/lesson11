package src;

public class Task implements Runnable {

    private final int number;

    public Task(int number) {
        this.number = number;
    }

    @Override
    public void run() {
        System.out.println("Задача " + number + " выполняется");
    }
}

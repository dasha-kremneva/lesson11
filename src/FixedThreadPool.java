package src;

import java.util.concurrent.LinkedBlockingQueue;

public class FixedThreadPool  implements ThreadPool{

    private final LinkedBlockingQueue queue;

    public FixedThreadPool(int nThreads){
        queue = new LinkedBlockingQueue();
        for (int i = 0; i < nThreads; i++) {
            start();
        }
    }


    @Override
    public void start() {
        new Thread(() -> {
            Runnable task;
            while (true) {
                synchronized (queue) {
                    while (queue.isEmpty()) {
                        try {
                            queue.wait();
                        } catch (InterruptedException e) {
                            System.out.println("Произошла ошибка во время ожидания очереди: " + e.getMessage());
                        }
                    }
                    task = (Runnable) queue.poll();
                }

                try {
                    task.run();
                } catch (RuntimeException e) {
                    System.out.println("Пул потоков прерван из-за ошибки: " + e.getMessage());
                }
            }
        }).start();
    }


    @Override
    public void execute(Runnable task) {
        synchronized (queue){
            queue.add(task);
            queue.notify();
        }
    }

}

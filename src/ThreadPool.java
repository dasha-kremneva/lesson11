package src;

public interface ThreadPool {

    //Запускает потоки. Потоки бездействуют пока не появится новое задание в очереди
    void start();

    //складывает задание в очередь, освободившийся поток должен выполнить это задание.
    // Каждое задание должно быть выполнено ровно 1 раз
    void execute(Runnable runnable);
}

package src;

import java.util.concurrent.LinkedBlockingQueue;

public class ScalableThreadPool implements ThreadPool {

    int currentPoolSize = 0;
    int corePoolSize;
    int maxPoolSize;
    private final LinkedBlockingQueue queue;

    public ScalableThreadPool(int corePoolSize, int maxPoolSize) {
        this.corePoolSize = corePoolSize;
        this.maxPoolSize = maxPoolSize;
        queue = new LinkedBlockingQueue();
        while(currentPoolSize < corePoolSize){
            start();
            currentPoolSize++;
        }
    }


    @Override
    public void start() {
        Thread thread = new Thread(new Runnable() {
            boolean flag = true;
            @Override
            public void run() {
                Runnable task;
                while (flag) {
                    synchronized (queue) {
                        while (queue.isEmpty()) {
                            try {
                                queue.wait();
                            } catch (InterruptedException e) {
                                System.out.println("Произошла ошибка во время ожидания очереди: " + e.getMessage());
                            }
                        }
                        task = (Runnable) queue.poll();
                    }
                    try {
                        task.run();
                        if(currentPoolSize > corePoolSize) {
                            flag = false;
                            currentPoolSize--;
                        }
                    } catch (RuntimeException e) {
                        System.out.println("Пул потоков прерван из-за ошибки: " + e.getMessage());
                    }
                }
            }
        });
        thread.start();
        thread.interrupt();
    }


    @Override
    public void execute(Runnable task) {
        synchronized (queue) {
            if(currentPoolSize < queue.size() & currentPoolSize < maxPoolSize) {
                start();
                currentPoolSize++;
            }
            queue.add(task);
            queue.notify();
        }
    }
}
